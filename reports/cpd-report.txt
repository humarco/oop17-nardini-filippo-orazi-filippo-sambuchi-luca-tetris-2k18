Found a 16 line (109 tokens) duplication in the following files: 
Starting at line 103 of /home/danysk/git/oop17-nardini-filippo-orazi-filippo-sambuchi-luca-tetris-2k18/src/main/java/test/Test.java
Starting at line 139 of /home/danysk/git/oop17-nardini-filippo-orazi-filippo-sambuchi-luca-tetris-2k18/src/main/java/test/Test.java

	public void testCollision() {
		final TetrisImpl t = new TetrisImpl(10, 20);
		t.setTetrominoCurrent(Shape.I);

		/* Anchor */
		for (int i = 0; i < 20; i++) {
			t.move(DOWN);
		}
		assertEquals(t.getAnchoredPieces(),
				Arrays.asList(new Square(3, 19, SquareColor.AZURE), 
				              new Square(4, 19, SquareColor.AZURE),
				              new Square(5, 19, SquareColor.AZURE), 
				              new Square(6, 19, SquareColor.AZURE)));

		/* the tetromino is anchor and start a new tetromino */
		t.setTetrominoCurrent(Shape.I);
=====================================================================
Found a 9 line (89 tokens) duplication in the following files: 
Starting at line 29 of /home/danysk/git/oop17-nardini-filippo-orazi-filippo-sambuchi-luca-tetris-2k18/src/main/java/test/Test.java
Starting at line 77 of /home/danysk/git/oop17-nardini-filippo-orazi-filippo-sambuchi-luca-tetris-2k18/src/main/java/test/Test.java

	public void testIsoutOfBounsMove() {
		final TetrisImpl t = new TetrisImpl(10, 20);
		t.setTetrominoCurrent(Shape.I);
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(3, 0, SquareColor.AZURE), 
				              new Square(4, 0, SquareColor.AZURE),
				              new Square(5, 0, SquareColor.AZURE), 
				              new Square(6, 0, SquareColor.AZURE)));
		t.move(RIGHT);
=====================================================================
Found a 7 line (70 tokens) duplication in the following files: 
Starting at line 31 of /home/danysk/git/oop17-nardini-filippo-orazi-filippo-sambuchi-luca-tetris-2k18/src/main/java/test/Test.java
Starting at line 85 of /home/danysk/git/oop17-nardini-filippo-orazi-filippo-sambuchi-luca-tetris-2k18/src/main/java/test/Test.java

		t.setTetrominoCurrent(Shape.I);
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(3, 0, SquareColor.AZURE), 
				              new Square(4, 0, SquareColor.AZURE),
				              new Square(5, 0, SquareColor.AZURE), 
				              new Square(6, 0, SquareColor.AZURE)));
		t.move(RIGHT);
=====================================================================
Found a 7 line (68 tokens) duplication in the following files: 
Starting at line 79 of /home/danysk/git/oop17-nardini-filippo-orazi-filippo-sambuchi-luca-tetris-2k18/src/main/java/test/Test.java
Starting at line 85 of /home/danysk/git/oop17-nardini-filippo-orazi-filippo-sambuchi-luca-tetris-2k18/src/main/java/test/Test.java

		t.setTetrominoCurrent(Shape.I);
		assertEquals(t.getCurrentTetromino().getAllSquares(),
				Arrays.asList(new Square(3, 0, SquareColor.AZURE), 
				              new Square(4, 0, SquareColor.AZURE),
				              new Square(5, 0, SquareColor.AZURE), 
				              new Square(6, 0, SquareColor.AZURE)));
		t.rotate(CLOCKWISE);
