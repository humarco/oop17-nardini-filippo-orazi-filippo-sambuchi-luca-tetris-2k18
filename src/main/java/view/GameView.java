package main.java.view;

import java.util.List;

import main.java.model.Input;
import main.java.model.Shape;
import main.java.model.Square;

/**
 * Interface for the View component of the MVC pattern.
 * It allows to render all the information of the model state.
 */
interface GameView {
	
  /**
   * Updates the current score.
   */
	void updateScore(int score);
  
	/**
   * Updates the number of lines cleared.
   */
	void updateLines(int lines);
	
	 /**
   * Updates the current level.
   */
	void updateLevel(int level);
	
	/**
	 * Draws the game board.
	 */
	void updateBoard(List<Square> board);
	
	/**
	 * Updates the next tetromino in the queue.
	 */
	void updateNext(Shape next);
	
	/**
	 * Updates the tetromino used by the hold power-up.
	 */
	void updateHold(Shape shape);
	
	/**
	 * Returns a list of the inputs that the view component catches.
	 */
	List<Input> getInputs();
}
