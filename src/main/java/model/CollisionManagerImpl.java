package main.java.model;

import java.util.ArrayList;
import java.util.List;

public class CollisionManagerImpl implements CollisionManager {

	private List<Square> temp = new ArrayList<>();
	private List<Square> board = new ArrayList<>();

	public CollisionManagerImpl() {
	}

	@Override
	public boolean check(final List<Square> board, final Tetromino t, final int height) {
		this.board = board;
		temp = t.getAllSquares();
		return temp.stream().filter(y -> y.getCoords().getY() >= height  
		                                  || (containsSquare(y))).count() > 0;
	}

	/** 
	 * Checks if the board contains a Square.
	 * @param s check if the square is inside the board.
	 * 
	 */
	private boolean containsSquare(final Square s) {
		return board.stream().filter(
				b -> b.getCoords().getX() == s.getCoords().getX() 
				            && b.getCoords().getY() == s.getCoords().getY())
				.count() > 0;

	}

}
