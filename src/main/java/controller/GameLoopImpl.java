package main.java.controller;

import static main.java.view.Dimension.WIDTH;

import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.util.Duration;
import main.java.model.GameEvent;
import main.java.model.Input;
import main.java.model.Tetris;
import main.java.view.Displayable;
import main.java.view.GameOverView;
import main.java.view.GameViewImpl;
import main.java.view.HighScoreForm;

/**
 * the GameLoop class manages the life of the game: calls the model Update and
 * the view render, giving them the information they need. Uses a defined max
 * frame rate set at 60 FPS, the view and the Model change at this rate.
 * 
 */
public class GameLoopImpl extends Thread implements GameLoop {
	private static final double frameRate = 1000.0 / 60;

	private boolean running;
	private boolean paused;
	private final Tetris game;
	private GameViewImpl view;
	private final Stage stage;
	private final EventDispatcher eventDispatcher;

	/**
	 * class constructor.
	 * 
	 * @param game  the Model reference
	 * @param stage the stage used for the view
	 */
	public GameLoopImpl(Tetris game, Stage stage) {
		super();
		this.game = game;
		this.view = new GameViewImpl(this, stage);
		this.eventDispatcher = new EventDispatcher() {

			private Timeline screenShake = new Timeline(new KeyFrame(
					Duration.millis(50), e -> {
				if (stage.getX() % 2 == 0) {
					stage.setX(stage.getX() + WIDTH.getValue() / 100);
				} else {
					stage.setX(stage.getX() - WIDTH.getValue() / 100);
				}
			}));

			@Override
			public void dispatch(GameEvent event) {
				switch (event) {
				case LINE_CLEAR:
					Sound.LINE.playSound();
					screenShake.setCycleCount(10);
					screenShake.setAutoReverse(true);
					screenShake.play();
					break;
				case MOVE:
					Sound.MOVE.playSound();
					break;
				case ROTATE:
					Sound.ROTATE.playSound();
					break;
				case TETRIS:
					Sound.TETRIS.playSound();
					screenShake.setCycleCount(10);
					screenShake.setAutoReverse(true);
					screenShake.play();
					break;
				default:
					break;
				}
			}
		};
		this.paused = false;
		this.stage = stage;
	}
	
	@Override
	public void run() {
		double lastKnownTime = System.currentTimeMillis();
		while (running) {

			double thisTime = System.currentTimeMillis();
			double elapsed = thisTime - lastKnownTime;
			lastKnownTime = thisTime;
			if (this.isPaused()) {
				elapsed = 0;
			}
			while (elapsed > 1 / frameRate) {
				List<Input> inputs = view.getInputs();
				game.sendInput(inputs);
				game.update(1 / frameRate);
				eventDispatcher.dispatchAll(game.getEvents());
				this.renderGame();
				elapsed -= (1 / frameRate);
				if (game.isGameOver()) {
					running = false;
				}
			}
		}
		gameOver();
	}

	public boolean isPaused() {
		return this.paused;
	}

	public boolean isRunning() {
		return this.running;
	}

	public void start() {
		super.start();
		this.running = true;
	}

	public void setView(GameViewImpl s) {
		this.view = s;
	}

	public void togglePause() {
		this.paused = !this.paused;
	}

	public Displayable getView() {
		return view;
	}

	/**
	 * manages what happens after the end game.
	 * 
	 */
	private void gameOver() {
		
		if (new FileManagement()
				.listCurrent()
				.stream()
				.filter(t -> t.getScore() > game.getScore()).count() < 10) {
			Platform.runLater(() -> stage.setScene(
					new HighScoreForm(stage, game.getScore()).getScene()));

		} else {
			Platform.runLater(() -> stage.setScene(
					new GameOverView(stage).getScene()));
		}
		this.interrupt();
	}

	/**
	 * calls the method to refresh the view.
	 * 
	 */
	private void renderGame() {
		Platform.runLater(() -> {
			view.updateScore(game.getScore());
			view.updateLines(game.getLines());
			view.updateLevel(game.getCurrentLevel());
			view.updateBoard(game.getBoard());
			view.updateNext(game.getNext().get(0));
			if (game.getHold().isPresent()) {
				view.updateHold(game.getHold().get());
			}
		});
	}

}